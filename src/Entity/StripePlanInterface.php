<?php

namespace AppPaymentClient\Entity;

interface StripePlanInterface
{
    public const YEARLY_PLAN = 1;
    public const MONTHLY_PLAN = 2;
    public const ME_PAGE_YEARLY_PLAN = 3;
    public const ME_PAGE_MONTHLY_PLAN = 4;
    public const ME_MENU_MONTHLY_PLAN = 5;
    public const ME_QR_LITE_PLAN = 6;
    public const ME_QR_MONTHLY_PLAN = 7;
    public const ME_QR_YEARLY_PLAN = 8;
    public const ME_MENU_PDF_PREMIUM = 9;
    public const ME_QR_LITE_YEARLY_PLAN = 13;
    public const ME_POS_LITE_MONTHLY_PLAN = 14;
    public const ME_POS_LITE_YEARLY_PLAN = 15;
    public const ME_POS_PREMIUM_MONTHLY_PLAN = 16;
    public const ME_POS_PREMIUM_YEARLY_PLAN = 17;
    public const ME_POS_WAREHOUSE_MONTHLY_PLAN = 18;
    public const ME_POS_WAREHOUSE_YEARLY_PLAN = 19;
    public const ME_WEB_SITE_PREMIUM_MONTHLY_PLAN = 20;
    public const ME_WEB_SITE_PREMIUM_YEARLY_PLAN = 21;
    public const ME_QR_100MB_STORAGE = 10;
    public const ME_QR_300MB_STORAGE = 11;
    public const ME_QR_500MB_STORAGE = 12;
    public const ME_QR_1GB_STORAGE = 22;
    public const ME_QR_10GB_STORAGE = 23;
    public const ME_QR_100GB_STORAGE = 24;
    public const ME_QR_1TB_STORAGE = 25;
    public const ME_QR_10TB_STORAGE = 26;
    public const ME_QR_100MB_STORAGE_YEARLY = 27;
    public const ME_QR_300MB_STORAGE_YEARLY = 28;
    public const ME_QR_500MB_STORAGE_YEARLY = 29;
    public const ME_QR_1GB_STORAGE_YEARLY = 30;
    public const ME_QR_10GB_STORAGE_YEARLY = 31;
    public const ME_QR_100GB_STORAGE_YEARLY = 32;
    public const ME_QR_1TB_STORAGE_YEARLY = 33;
    public const ME_QR_10TB_STORAGE_YEARLY = 34;
    public const ME_TICKET_PRO_MONTHLY_PLAN = 35;
    public const ME_TICKET_PRO_YEARLY_PLAN = 36;
    public const ME_QR_STORAGE = 37;
    public const ME_QR_STORAGE_YEARLY = 38;
}
