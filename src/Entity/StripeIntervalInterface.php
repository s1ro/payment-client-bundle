<?php

namespace AppPaymentClient\Entity;

interface StripeIntervalInterface
{
    public const DAY = 'day';
    public const WEEK = 'week';
    public const MONTH = 'month';
    public const YEAR = 'year';
}