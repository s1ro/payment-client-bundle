<?php

namespace AppPaymentClient\Service\Stripe\Connect;

use AppPaymentClient\Service\Stripe\Connect\DTO\StripeConnectCountryDTO;

class StripeConnectCountryProvider
{
    private const COUNTRIES = [
//        'AU' => 'Australia',
        'AT' => 'Austria',
        'BE' => 'Belgium',
//        'BR' => 'Brazil',
        'BG' => 'Bulgaria',
//        'CA' => 'Canada',
        'HR' => 'Croatia',
        'CY' => 'Cyprus',
        'CZ' => 'Czech Republic',
        'DK' => 'Denmark',
        'EE' => 'Estonia',
        'FI' => 'Finland',
        'FR' => 'France',
        'DE' => 'Germany',
        'GI' => 'Gibraltar',
        'GR' => 'Greece',
//        'HK' => 'Hong Kong',
        'HU' => 'Hungary',
//        'IN' => 'India',
        'IE' => 'Ireland',
        'IT' => 'Italy',
//        'IS' => 'Iceland',
//        'JP' => 'Japan',
        'LV' => 'Latvia',
        'LI' => 'Liechtenstein',
        'LT' => 'Lithuania',
        'LU' => 'Luxembourg',
//        'MY' => 'Malaysia',
        'MT' => 'Malta',
//        'MX' => 'Mexico',
        'NL' => 'Nederland',
//        'NZ' => 'New Zealand',
        'NO' => 'Norway',
        'PL' => 'Poland',
        'PT' => 'Portugal',
        'RO' => 'Romania',
//        'SG' => 'Singapore',
        'SK' => 'Slovakia',
        'SI' => 'Slovenia',
        'ES' => 'Spain',
        'SE' => 'Sweden',
        'CH' => 'Switzerland',
//        'TH' => 'Thailand',
//        'AE' => 'United Arab Emirates',
        'GB' => 'United Kingdom',
//        'US' => 'United States',
    ];

    /**
     * @return StripeConnectCountryDTO[]
     */
    public static function getCountries(): array
    {
        $result = [];

        foreach (self::COUNTRIES as $code => $name) {
            $result[] = new StripeConnectCountryDTO($code, $name);
        }

        return $result;
    }

    /**
     * @return string[]
     */
    public static function getCountriesAsArray(): array
    {
        return self::COUNTRIES;
    }
}