<?php

namespace AppPaymentClient\Service\Stripe\Connect\DTO;

class StripeConnectCountryDTO
{
    /**
     * @var string
     */
    private $countryCode;
    /**
     * @var string
     */
    private $countryName;

    public function __construct(string $countryCode, string  $countryName)
    {
        $this->countryCode = $countryCode;
        $this->countryName = $countryName;
    }

    /**
     * @return string
     */
    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    /**
     * @return string
     */
    public function getCountryName(): string
    {
        return $this->countryName;
    }
}