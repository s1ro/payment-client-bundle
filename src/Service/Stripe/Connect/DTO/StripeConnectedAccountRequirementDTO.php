<?php

namespace AppPaymentClient\Service\Stripe\Connect\DTO;

class StripeConnectedAccountRequirementDTO
{
    /**
     * @var \DateTimeInterface|null
     */
    private $currentDeadline;
    /**
     * @var string[]|null
     */
    private $currentlyDue;
    /**
     * @var string|null
     */
    private $disableReason;
    /**
     * @var StripeConnectedAccountRequirementErrorDTO[]|null
     */
    private $errors;
    /**
     * @var string[]|null
     */
    private $eventuallyDue;
    /**
     * @var string[]|null
     */
    private $pastDue;
    /**
     * @var string[]|null
     */
    private $pendingVerification;

    public function __construct(
        ?\DateTimeInterface $currentDeadline,
        ?array $currentlyDue,
        ?string $disableReason,
        ?array $errors,
        ?array $eventuallyDue,
        ?array $pastDue,
        ?array $pendingVerification
    )
    {
        $this->currentDeadline = $currentDeadline;
        $this->currentlyDue = $currentlyDue;
        $this->disableReason = $disableReason;
        $this->errors = $errors;
        $this->eventuallyDue = $eventuallyDue;
        $this->pastDue = $pastDue;
        $this->pendingVerification = $pendingVerification;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCurrentDeadline(): ?\DateTimeInterface
    {
        return $this->currentDeadline;
    }

    /**
     * @return string[]|null
     */
    public function getCurrentlyDue(): ?array
    {
        return $this->currentlyDue;
    }

    /**
     * @return string|null
     */
    public function getDisableReason(): ?string
    {
        return $this->disableReason;
    }

    /**
     * @return StripeConnectedAccountRequirementErrorDTO[]|null
     */
    public function getErrors(): ?array
    {
        return $this->errors;
    }

    /**
     * @return string[]|null
     */
    public function getEventuallyDue(): ?array
    {
        return $this->eventuallyDue;
    }

    /**
     * @return string[]|null
     */
    public function getPastDue(): ?array
    {
        return $this->pastDue;
    }

    /**
     * @return string[]|null
     */
    public function getPendingVerification(): ?array
    {
        return $this->pendingVerification;
    }
}
