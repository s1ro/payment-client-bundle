<?php

namespace AppPaymentClient\Service\Stripe\Connect\DTO;

class StripeConnectedAccountRequirementErrorDTO
{
    /**
     * @var string
     */
    private $code;
    /**
     * @var string
     */
    private $reason;
    /**
     * @var string
     */
    private $requirement;

    public function __construct(string $code, string $reason, string $requirement)
    {
        $this->code = $code;
        $this->reason = $reason;
        $this->requirement = $requirement;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @return string
     */
    public function getRequirement(): string
    {
        return $this->requirement;
    }
}
