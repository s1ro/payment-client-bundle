<?php

namespace AppPaymentClient\Service\Stripe\Connect\BankAccount;

use AppPaymentClient\Service\Stripe\Connect\BankAccount\DTO\StripeConnectBankAccountCountryDTO;

class StripeConnectBankAccountCountryProvider
{
    private const DEFAULT_MULTI_CURRENCY = ['AUD', 'CAD', 'CHF', 'CZK', 'DKK', 'EUR', 'GBP', 'HKD', 'HUF', 'JPY', 'NOK', 'NZD', 'PLN', 'RON', 'SEK', 'SGD', 'USD', 'ZAR'];

    private const COUNTRIES = [
        'AL' => [
            'name' => 'Albania',
            'currencies' => ['ALL'],
            'iban' => true,
            'swift' => true,
        ],
        'DZ' => [
            'name' => 'Algeria',
            'currencies' => ['DZD'],
            'swift' => true,
            'number' => true,
        ],
        'AO' => [
            'name' => 'Angola',
            'currencies' => ['AOA'],
            'swift' => true,
            'iban' => true,
        ],
        'AG' => [
            'name' => 'Antigua and Barbuda',
            'currencies' => ['XCD'],
            'swift' => true,
            'number' => true,
        ],
        'AR' => [
            'name' => 'Argentina',
            'currencies' => ['ARS'],
            'number' => true,
        ],
        'AM' => [
            'name' => 'Armenia',
            'currencies' => ['AMD'],
            'swift' => true,
            'number' => true,
        ],
        'AU' => [
            'name' => 'Australia',
            'currencies' => ['AUD'],
            'routing' => true,
            'number' => true,
        ],
        'AT' => [
            'name' => 'Austria',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'AZ' => [
            'name' => 'Azerbaijan',
            'currencies' => ['AZN'],
            'routing' => true,
            'iban' => true,
        ],
        'BS' => [
            'name' => 'Bahamas',
            'currencies' => ['BSD'],
            'swift' => true,
            'number' => true,
        ],
        'BH' => [
            'name' => 'Bahrain',
            'currencies' => ['BHD'],
            'swift' => true,
            'iban' => true,
        ],
        'BD' => [
            'name' => 'Bangladesh',
            'currencies' => ['BDT'],
            'routing' => true,
            'number' => true,
        ],
        'BE' => [
            'name' => 'Belgium',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'BJ' => [
            'name' => 'Benin',
            'currencies' => ['XOF'],
            'iban' => true,
        ],
        'BT' => [
            'name' => 'Bhutan',
            'currencies' => ['BTN'],
            'swift' => true,
            'number' => true,
        ],
        'BO' => [
            'name' => 'Bolivia',
            'currencies' => ['BOB'],
            'number' => true,
        ],
        'BA' => [
            'name' => 'Bosnia and Herzegovina',
            'currencies' => ['BAM'],
            'swift' => true,
            'iban' => true,
        ],
        'BW' => [
            'name' => 'Botswana',
            'currencies' => ['BWP'],
            'swift' => true,
            'number' => true,
        ],
        'BR' => [
            'name' => 'Brazil',
            'currencies' => ['BRL'],
            'routing' => true,
            'number' => true,
        ],
        'BN' => [
            'name' => 'Brunei',
            'currencies' => ['BND'],
            'swift' => true,
            'number' => true,
        ],
        'BG' => [
            'name' => 'Bulgaria',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'KH' => [
            'name' => 'Cambodia',
            'currencies' => ['KHR'],
            'swift' => true,
            'number' => true,
        ],
        'CA' => [
            'name' => 'Canada',
            'currencies' => ['CAD'],
            'routing' => true,
            'number' => true,
        ],
        'CL' => [
            'name' => 'Chile',
            'currencies' => ['CLP'],
            'routing' => true,
            'number' => true,
        ],
        'CO' => [
            'name' => 'Colombia',
            'currencies' => ['COP'],
            'routing' => true,
            'number' => true,
        ],
        'CR' => [
            'name' => 'Costa Rica',
            'currencies' => ['CRC'],
            'iban' => true,
        ],
        'CI' => [
            'name' => 'Côte d\'Ivoire',
            'currencies' => ['XOF'],
            'iban' => true,
        ],
        'HR' => [
            'name' => 'Croatia',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'CY' => [
            'name' => 'Cyprus',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'CZ' => [
            'name' => 'Czech Republic',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'DK' => [
            'name' => 'Denmark',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'DO' => [
            'name' => 'Dominican Republic',
            'currencies' => ['DOP'],
            'routing' => true,
            'number' => true,
        ],
        'EC' => [
            'name' => 'Ecuador',
            'currencies' => ['USD'],
            'swift' => true,
            'number' => true,
        ],
        'EG' => [
            'name' => 'Egypt',
            'currencies' => ['EGP'],
            'routing' => true,
            'iban' => true,
        ],
        'SV' => [
            'name' => 'El Salvador',
            'currencies' => ['USD'],
            'swift' => true,
            'iban' => true,
        ],
        'EE' => [
            'name' => 'Estonia',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'ET' => [
            'name' => 'Ethiopia',
            'currencies' => ['ETB'],
            'swift' => true,
            'number' => true,
        ],
        'FI' => [
            'name' => 'Finland',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'FR' => [
            'name' => 'France',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'GA' => [
            'name' => 'Gabon',
            'currencies' => ['XAF'],
            'swift' => true,
            'number' => true,
        ],
        'GM' => [
            'name' => 'Gambia',
            'currencies' => ['GMD'],
            'swift' => true,
            'number' => true,
        ],
        'GE' => [
            'name' => 'Georgia',
            'currencies' => ['GEL'],
            'swift' => true,
            'number' => true,
        ],
        'DE' => [
            'name' => 'Germany',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'GH' => [
            'name' => 'Ghana',
            'currencies' => ['GHS'],
            'routing' => true,
            'number' => true,
        ],
        'GI' => [
            'name' => 'Gibraltar',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'GR' => [
            'name' => 'Greece',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'GT' => [
            'name' => 'Guatemala',
            'currencies' => ['GTQ'],
            'swift' => true,
            'iban' => true,
        ],
        'GY' => [
            'name' => 'Guyana',
            'currencies' => ['GYD'],
            'swift' => true,
            'number' => true,
        ],
        'HK' => [
            'name' => 'Hong Kong',
            'currencies' => ['HKD'],
            'routing' => true,
            'number' => true,
        ],
        'HU' => [
            'name' => 'Hungary',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'IS' => [
            'name' => 'Iceland',
            'currencies' => ['EUR'],
            'iban' => true,
        ],
        'IN' => [
            'name' => 'India',
            'currencies' => ['INR'],
            'routing' => true,
            'number' => true,
        ],
        'ID' => [
            'name' => 'Indonesia',
            'currencies' => ['IDR'],
            'routing' => true,
            'number' => true,
        ],
        'IE' => [
            'name' => 'Ireland',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'IL' => [
            'name' => 'Israel',
            'currencies' => ['ILS'],
            'iban' => true,
        ],
        'IT' => [
            'name' => 'Italy',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'JM' => [
            'name' => 'Jamaica',
            'currencies' => ['JMD'],
            'routing' => true,
            'number' => true,
        ],
        'JP' => [
            'name' => 'Japan',
            'currencies' => ['JPY'],
            'routing' => true,
            'number' => true,
        ],
        'JO' => [
            'name' => 'Jordan',
            'currencies' => ['JOD'],
            'swift' => true,
            'iban' => true,
        ],
        'KZ' => [
            'name' => 'Kazakhstan',
            'currencies' => ['KZT'],
            'swift' => true,
            'iban' => true,
        ],
        'KE' => [
            'name' => 'Kenya',
            'currencies' => ['KES'],
            'swift' => true,
            'number' => true,
        ],
        'KW' => [
            'name' => 'Kuwait',
            'currencies' => ['KWD'],
            'swift' => true,
            'iban' => true,
        ],
        'LA' => [
            'name' => 'Laos',
            'currencies' => ['LAK'],
            'swift' => true,
            'number' => true,
        ],
        'LV' => [
            'name' => 'Latvia',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'LI' => [
            'name' => 'Liechtenstein',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'LT' => [
            'name' => 'Lithuania',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'LU' => [
            'name' => 'Luxembourg',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'MO' => [
            'name' => 'Macao',
            'currencies' => ['MOP'],
            'swift' => true,
            'number' => true,
        ],
        'MG' => [
            'name' => 'Madagascar',
            'currencies' => ['MGA'],
            'swift' => true,
            'iban' => true,
        ],
        'MY' => [
            'name' => 'Malaysia',
            'currencies' => ['MYR'],
            'swift' => true,
            'number' => true,
        ],
        'MT' => [
            'name' => 'Malta',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'MU' => [
            'name' => 'Mauritius',
            'currencies' => ['MUR'],
            'iban' => true,
        ],
        'MX' => [
            'name' => 'Mexico',
            'currencies' => ['MXN'],
            'number' => true,
        ],
        'MD' => [
            'name' => 'Moldova',
            'currencies' => ['MDL'],
            'swift' => true,
            'iban' => true,
        ],
        'MC' => [
            'name' => 'Monaco',
            'currencies' => ['EUR'],
            'iban' => true,
        ],
        'MN' => [
            'name' => 'Mongolia',
            'currencies' => ['MNT'],
            'swift' => true,
            'number' => true,
        ],
        'MA' => [
            'name' => 'Morocco',
            'currencies' => ['MAD'],
            'swift' => true,
            'iban' => true,
        ],
        'MZ' => [
            'name' => 'Mozambique',
            'currencies' => ['MZN'],
            'swift' => true,
            'number' => true,
        ],
        'NA' => [
            'name' => 'Namibia',
            'currencies' => ['NAD'],
            'swift' => true,
            'number' => true,
        ],
        'NL' => [
            'name' => 'Netherlands',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'NZ' => [
            'name' => 'New Zealand',
            'currencies' => ['NZD'],
            'number' => true,
        ],
        'NE' => [
            'name' => 'Niger',
            'currencies' => ['XOF'],
            'iban' => true,
        ],
        'NG' => [
            'name' => 'Nigeria',
            'currencies' => ['NGN'],
            'swift' => true,
            'number' => true,
        ],
        'MK' => [
            'name' => 'North Macedonia',
            'currencies' => ['MKD'],
            'swift' => true,
            'iban' => true,
        ],
        'NO' => [
            'name' => 'Norway',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'OM' => [
            'name' => 'Oman',
            'currencies' => ['OMR'],
            'swift' => true,
            'number' => true,
        ],
        'PK' => [
            'name' => 'Pakistan',
            'currencies' => ['PKR'],
            'swift' => true,
            'iban' => true,
        ],
        'PA' => [
            'name' => 'Panama',
            'currencies' => ['USD'],
            'swift' => true,
            'number' => true,
        ],
        'PY' => [
            'name' => 'Paraguay',
            'currencies' => ['PYG'],
            'routing' => true,
            'number' => true,
        ],
        'PE' => [
            'name' => 'Peru',
            'currencies' => ['PEN'],
            'number' => true,
        ],
        'PH' => [
            'name' => 'Philippines',
            'currencies' => ['PHP'],
            'swift' => true,
            'number' => true,
        ],
        'PL' => [
            'name' => 'Poland',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'PT' => [
            'name' => 'Portugal',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'QA' => [
            'name' => 'Qatar',
            'currencies' => ['QAR'],
            'swift' => true,
            'iban' => true,
        ],
        'RO' => [
            'name' => 'Romania',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'RW' => [
            'name' => 'Rwanda',
            'currencies' => ['RWF'],
            'swift' => true,
            'number' => true,
        ],
        'SM' => [
            'name' => 'San Marino',
            'currencies' =>  ['EUR'],
            'swift' => true,
            'iban' => true,
        ],
        'SA' => [
            'name' => 'Saudi Arabia',
            'currencies' => ['SAR'],
            'swift' => true,
            'iban' => true,
        ],
        'SN' => [
            'name' => 'Senegal',
            'currencies' => ['XOF'],
            'iban' => true,
        ],
        'RS' => [
            'name' => 'Serbia',
            'currencies' => ['RSD'],
            'swift' => true,
            'iban' => true,
        ],
        'SG' => [
            'name' => 'Singapore',
            'currencies' => ['SGD'],
            'routing' => true,
            'number' => true,
        ],
        'SK' => [
            'name' => 'Slovakia',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'SI' => [
            'name' => 'Slovenia',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'ZA' => [
            'name' => 'South Africa',
            'currencies' => ['ZAR'],
            'swift' => true,
            'number' => true,
        ],
        'KR' => [
            'name' => 'South Korea',
            'currencies' => ['KRW'],
            'swift' => true,
            'number' => true,
        ],
        'ES' => [
            'name' => 'Spain',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'LK' => [
            'name' => 'Sri Lanka',
            'currencies' => ['LKR'],
            'routing' => true,
            'number' => true,
        ],
        'LC' => [
            'name' => 'Saint Lucia',
            'currencies' => ['XCD'],
            'swift' => true,
            'number' => true,
        ],
        'SE' => [
            'name' => 'Sweden',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'CH' => [
            'name' => 'Switzerland',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
        ],
        'TW' => [
            'name' => 'Taiwan',
            'currencies' => ['TWD'],
            'swift' => true,
            'number' => true,
        ],
        'TZ' => [
            'name' => 'Tanzania',
            'currencies' => ['TZS'],
            'swift' => true,
            'number' => true,
        ],
        'TH' => [
            'name' => 'Thailand',
            'currencies' => ['THB'],
            'routing' => true,
            'number' => true,
        ],
        'TT' => [
            'name' => 'Trinidad and Tobago',
            'currencies' => ['TTD'],
            'routing' => true,
            'number' => true,
        ],
        'TN' => [
            'name' => 'Tunisia',
            'currencies' => ['TND'],
            'iban' => true,
        ],
        'TR' => [
            'name' => 'Turkey',
            'currencies' => ['TRY'],
            'iban' => true,
        ],
        'AE' => [
            'name' => 'United Arab Emirates',
            'currencies' => ['AED'],
            'iban' => true,
        ],
        'GB' => [
            'name' => 'United Kingdom',
            'currencies' => self::DEFAULT_MULTI_CURRENCY,
            'iban' => true,
            'alt' => ['sort_code', 'number'],
        ],
        'US' => [
            'name' => 'United States',
            'currencies' => ['USD'],
            'routing' => true,
            'number' => true,
        ],
        'UY' => [
            'name' => 'Uruguay',
            'currencies' => ['UYU'],
            'routing' => true,
            'number' => true,
        ],
        'UZ' => [
            'name' => 'Uzbekistan',
            'currencies' => ['UZS'],
            'routing' => true,
            'number' => true,
        ],
        'VN' => [
            'name' => 'Vietnam',
            'currencies' => ['VND'],
            'routing' => true,
            'number' => true,
        ],
    ];

    /**
     * @return StripeConnectBankAccountCountryDTO[]
     */
    public static function getCountries(): array
    {
        $result = [];
        foreach (self::COUNTRIES as $countryCode => $data) {
            $result[] = new StripeConnectBankAccountCountryDTO(
                $countryCode,
                $data['name'],
                $data['currencies'],
                $data['routing'] ?? false,
                $data['swift'] ?? false,
                $data['number'] ?? false,
                $data['iban'] ?? false,
                $data['alt'] ?? null
            );
        }

        return $result;
    }

    public static function getCountriesAsArray(): array
    {
        return self::COUNTRIES;
    }
}