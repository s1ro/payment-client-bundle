<?php

namespace AppPaymentClient\Service\Stripe\Connect\BankAccount\DTO;

class StripeConnectBankAccountCountryDTO
{
    /**
     * @var string
     */
    private $countryCode;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string[]
     */
    private $currencies;
    /**
     * @var bool
     */
    private $routing;
    /**
     * @var bool
     */
    private $swift;
    /**
     * @var bool
     */
    private $number;
    /**
     * @var bool
     */
    private $iban;
    /**
     * @var string[]|null
     */
    private $alt;

    /**
     * @param string[] $currencies
     * @param string[]|null $alt
     */
    public function __construct(string $countryCode, string $name, array $currencies, bool $routing, bool $swift, bool $number, bool $iban, ?array $alt)
    {
        $this->countryCode = $countryCode;
        $this->name = $name;
        $this->currencies = $currencies;
        $this->routing = $routing;
        $this->swift = $swift;
        $this->number = $number;
        $this->iban = $iban;
        $this->alt = $alt;
    }

    /**
     * @return string
     */
    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string[]
     */
    public function getCurrencies(): array
    {
        return $this->currencies;
    }

    /**
     * @return bool
     */
    public function isRouting(): bool
    {
        return $this->routing;
    }

    /**
     * @return bool
     */
    public function isSwift(): bool
    {
        return $this->swift;
    }

    /**
     * @return bool
     */
    public function isNumber(): bool
    {
        return $this->number;
    }

    /**
     * @return bool
     */
    public function isIban(): bool
    {
        return $this->iban;
    }

    /**
     * @return string[]|null
     */
    public function getAlt(): ?array
    {
        return $this->alt;
    }
}