<?php

namespace AppPaymentClient\Service\Stripe\Customer\DTO;

class CustomerAddressDTO
{
    /**
     * @var string|null
     */
    private $country;

    public function __construct(?string $country)
    {
        $this->country = $country;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }
}