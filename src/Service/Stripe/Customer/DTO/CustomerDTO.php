<?php

namespace AppPaymentClient\Service\Stripe\Customer\DTO;

class CustomerDTO
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string|null
     */
    private $email;
    /**
     * @var CustomerAddressDTO|null
     */
    private $address;

    public function __construct(string $id, ?string $email, ?CustomerAddressDTO $address)
    {
        $this->id = $id;
        $this->email = $email;
        $this->address = $address;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getAddress(): ?CustomerAddressDTO
    {
        return $this->address;
    }
}
