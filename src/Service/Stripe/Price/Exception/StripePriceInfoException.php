<?php

namespace AppPaymentClient\Service\Stripe\Price\Exception;

class StripePriceInfoException extends \Exception
{
    /**
     * @return self
     */
    public static function createOrderDataNotFound(): self
    {
        return new self('Order data not found!');
    }

    /**
     * @param string $message
     * @return self
     */
    public static function create(string $message): self
    {
        return new self($message);
    }

    /**
     * @param \Throwable $t
     * @return self
     */
    public static function fromThrowable(\Throwable $t): self
    {
        return new self($t->getMessage(), $t->getCode(), $t);
    }
}
