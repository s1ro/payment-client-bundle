<?php

namespace AppPaymentClient\Service\Stripe\Price;

use AppPaymentClient\Service\Stripe\Price\DTO\CurrencyAmountDTO;
use AppPaymentClient\Service\Stripe\Price\DTO\PlanPriceDTO;
use AppPaymentClient\Service\Stripe\Price\DTO\PriceInfoDTO;
use AppPaymentClient\Service\Stripe\Price\Exception\StripePriceException;
use AppPaymentClient\Service\Stripe\Price\Exception\StripePriceInfoException;

interface StripePriceServiceInterface
{
    /**
     * @param int $plan
     * @param bool $test
     * @param string|null $countryCode
     * @return CurrencyAmountDTO[]
     * @throws StripePriceException
     */
    public function getCurrencyAmount(int $plan, bool $test = false, ?string $countryCode = null): array;

    /**
     * @param bool $test
     * @param string|null $countryCode
     * @return PlanPriceDTO[]
     * @throws StripePriceException
     */
    public function getServiceCurrencyAmounts(bool $test = false, ?string $countryCode = null): array;

    /**
     * @param int $orderId
     * @param int $planId
     * @return PriceInfoDTO[]
     * @throws StripePriceInfoException
     */
    public function getOrderPriceInfo(int $orderId, int $planId): array;
}
