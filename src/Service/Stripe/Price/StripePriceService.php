<?php

namespace AppPaymentClient\Service\Stripe\Price;

use AppPaymentClient\Service\AbstractClient;
use AppPaymentClient\Service\ServiceNameProvider;
use AppPaymentClient\Service\Stripe\Price\DTO\CurrencyAmountDTO;
use AppPaymentClient\Service\Stripe\Price\DTO\PlanPriceDTO;
use AppPaymentClient\Service\Stripe\Price\DTO\PriceInfoDTO;
use AppPaymentClient\Service\Stripe\Price\Exception\StripePriceException;
use AppPaymentClient\Service\Stripe\Price\Exception\StripePriceInfoException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class StripePriceService extends AbstractClient implements StripePriceServiceInterface
{
    private const BASE_URI = '/api/stripe/price';

    /**
     * @var HttpClientInterface
     */
    private $httpClient;
    /**
     * @var ServiceNameProvider
     */
    private $serviceNameProvider;

    public function __construct(HttpClientInterface $httpClient, ServiceNameProvider $serviceNameProvider)
    {
        $this->serviceNameProvider = $serviceNameProvider;
        $this->httpClient = $httpClient;
    }

    /**
     * @inheritDoc
     */
    public function getCurrencyAmount(int $plan, bool $test = false, ?string $countryCode = null): array
    {
        $query = [
            'plan' => $plan,
            'test' => (int) $test,
            'service_name' => $this->serviceNameProvider->getServiceName(),
        ];
        if (!is_null($countryCode)) {
            $query['country_code'] = strtoupper($countryCode);
        }
        $retries = 0;
        while (true) {
            try {
                $response = $this->httpClient->request(
                    'GET',
                    $this->getAppPaymentsUrl() . self::BASE_URI . '/currency-amount',
                    ['query' => $query]
                );
                $data = json_decode($response->getContent(), true);
            } catch (\Throwable $t) {
                if (++$retries >= 3) {
                    throw StripePriceException::fromThrowable($t);
                }
                sleep(1);
                continue;
            }
            break;
        }
        if (isset($data['error']) && $data['error']) {
            throw StripePriceException::make($data['message'] ?? 'Stripe error');
        }
        if (!isset($data['currency_amount'])) {
            throw StripePriceException::make('Invalid response from received from stripe');
        }
        $result = [];
        foreach ($data['currency_amount'] as $datum) {
            if (!isset($datum['currency'], $datum['amount'])) {
                continue;
            }
            $result[] = CurrencyAmountDTO::make($datum['currency'], $datum['amount']);
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getServiceCurrencyAmounts(bool $test = false, ?string $countryCode = null): array
    {
        $query = [
            'test' => (int) $test,
            'service_name' => $this->serviceNameProvider->getServiceName(),
        ];
        if (!is_null($countryCode)) {
            $query['country_code'] = strtoupper($countryCode);
        }
        try {
            $response = $this->httpClient->request(
                'GET',
                $this->getAppPaymentsUrl() . self::BASE_URI . '/service-currency-amount',
                ['query' => $query]
            );
            $data = json_decode($response->getContent(false), true);
        } catch (\Throwable $t) {
            throw StripePriceException::fromThrowable($t);
        }
        if (isset($data['error']) && $data['error']) {
            throw StripePriceException::make($data['message'] ?? 'Stripe price error');
        }
        if (!is_array($data)) {
            throw StripePriceException::make('Invalid response from stripe');
        }
        $result = [];
        foreach ($data as $planId => $priceData) {
            if (!is_array($priceData)) {
                continue;
            }
            $currencyAmounts = [];
            foreach ($priceData as $item) {
                if (!isset($item['currency'], $item['amount'])) {
                    continue;
                }
                $currencyAmounts[] = CurrencyAmountDTO::make($item['currency'], $item['amount']);
            }
            $result[] = new PlanPriceDTO($planId, $currencyAmounts);
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getOrderPriceInfo(int $orderId, int $planId): array
    {
        try {
            $response = $this->httpClient->request(
                'GET',
                $this->getAppPaymentsUrl() . self::BASE_URI . '/price-info/order/' . $orderId . '/plan/' . $planId,
                ['query' => ['service_name' => $this->serviceNameProvider->getServiceName()]]
            );
            $code = $response->getStatusCode();
            $data = json_decode($response->getContent(false), true);
        } catch (\Throwable $t) {
            throw StripePriceInfoException::fromThrowable($t);
        }
        if ($code === Response::HTTP_NOT_FOUND) {
            throw StripePriceInfoException::createOrderDataNotFound();
        }
        if (isset($data['error']) && $data['error']) {
            throw StripePriceInfoException::create($data['message'] ?? 'Stripe error!');
        }
        if (!isset($data['price_infos'])) {
            throw StripePriceInfoException::create('Invalid response from stripe!');
        }
        $result = [];
        foreach ($data['price_infos'] as $price_info) {
            $date = $price_info['date'] ?? null;
            $result[] = new PriceInfoDTO(
                $price_info['amount'] ?? null,
                $price_info['currecny'] ?? null,
                is_null($date) ? null : (new \DateTime())->setTimestamp($date)
            );
        }
        return $result;
    }
}
