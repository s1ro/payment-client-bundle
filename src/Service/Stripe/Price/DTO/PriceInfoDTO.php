<?php

namespace AppPaymentClient\Service\Stripe\Price\DTO;

class PriceInfoDTO
{
    /**
     * @var int|null
     */
    private $amount;
    /**
     * @var string|null
     */
    private $currency;
    /**
     * @var \DateTimeInterface|null
     */
    private $date;

    public function __construct(?int $amount, ?string $currency, ?\DateTimeInterface $date)
    {
        $this->amount = $amount;
        $this->currency = $currency;
        $this->date = $date;
    }

    /**
     * @return int|null
     */
    public function getAmount(): ?int
    {
        return $this->amount;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }
}
