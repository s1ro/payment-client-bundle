<?php

namespace AppPaymentClient\Service\Stripe\Price\DTO;

class PlanPriceDTO
{
    /**
     * @var int
     */
    private $planId;
    /**
     * @var CurrencyAmountDTO[]
     */
    private $currencyAmounts;

    public function __construct(int $id, array $currencyAmounts)
    {
        $this->planId = $id;
        $this->currencyAmounts = $currencyAmounts;
    }

    /**
     * @return int
     */
    public function getPlanId(): int
    {
        return $this->planId;
    }

    /**
     * @return CurrencyAmountDTO[]
     */
    public function getCurrencyAmounts(): array
    {
        return $this->currencyAmounts;
    }
}
