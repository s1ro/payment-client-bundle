<?php

namespace AppPaymentClient\Service\Stripe\Dispute;

use AppPaymentClient\Service\Stripe\Dispute\DTO\DisputeDTO;
use AppPaymentClient\Service\Stripe\Dispute\Exception\StripeDisputeException;

interface StripeDisputeServiceInterface
{
    /**
     * @param int $orderId
     * @param int $planId
     * @return DisputeDTO[]
     * @throws StripeDisputeException
     */
    public function getDisputesByOrder(int $orderId, int $planId): array;

    /**
     * @param string $id
     * @param bool $test
     * @return DisputeDTO|null
     * @throws StripeDisputeException
     */
    public function getDisputeById(string $id, bool $test = false): ?DisputeDTO;
}
