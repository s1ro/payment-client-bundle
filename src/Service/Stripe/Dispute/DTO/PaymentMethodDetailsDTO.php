<?php

namespace AppPaymentClient\Service\Stripe\Dispute\DTO;

class PaymentMethodDetailsDTO
{
    /**
     * @var CardDTO|null
     */
    private $card;
    /**
     * @var string|null
     */
    private $type;

    public function __construct(?CardDTO $card, ?string $type)
    {
        $this->card = $card;
        $this->type = $type;
    }

    /**
     * @return CardDTO|null
     */
    public function getCard(): ?CardDTO
    {
        return $this->card;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
}
