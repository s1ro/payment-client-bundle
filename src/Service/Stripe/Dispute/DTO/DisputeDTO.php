<?php

namespace AppPaymentClient\Service\Stripe\Dispute\DTO;

class DisputeDTO
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var int
     */
    private $amount;
    /**
     * @var \DateTimeInterface
     */
    private $created;
    /**
     * @var string
     */
    private $currency;
    /**
     * @var EvidenceDTO
     */
    private $evidence;
    /**
     * @var string
     */
    private $paymentIntentId;
    /**
     * @var PaymentMethodDetailsDTO|null
     */
    private $paymentMethodDetails;
    /**
     * @var string
     */
    private $reason;
    /**
     * @var string
     */
    private $status;

    public function __construct(
        string $id,
        int $amount,
        \DateTimeInterface $created,
        string $currency,
        EvidenceDTO $evidence,
        string $paymentIntentId,
        ?PaymentMethodDetailsDTO $paymentMethodDetails,
        string $reason,
        string $status
    )
    {
        $this->id = $id;
        $this->amount = $amount;
        $this->created = $created;
        $this->currency = $currency;
        $this->evidence = $evidence;
        $this->paymentIntentId = $paymentIntentId;
        $this->paymentMethodDetails = $paymentMethodDetails;
        $this->reason = $reason;
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return EvidenceDTO
     */
    public function getEvidence(): EvidenceDTO
    {
        return $this->evidence;
    }

    /**
     * @return string
     */
    public function getPaymentIntentId(): string
    {
        return $this->paymentIntentId;
    }

    /**
     * @return PaymentMethodDetailsDTO|null
     */
    public function getPaymentMethodDetails(): ?PaymentMethodDetailsDTO
    {
        return $this->paymentMethodDetails;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }
}
