<?php

namespace AppPaymentClient\Service\Stripe\Dispute\DTO;

class EvidenceDTO
{
    /**
     * @var string|null
     */
    private $billingAddress;
    /**
     * @var string|null
     */
    private $customerName;
    /**
     * @var string|null
     */
    private $customerEmail;
    /**
     * @var string|null
     */
    private $customerIp;

    public function __construct(
        ?string $billingAddress,
        ?string $customerName,
        ?string $customerEmail,
        ?string $customerIp
    )
    {
        $this->billingAddress = $billingAddress;
        $this->customerName = $customerName;
        $this->customerEmail = $customerEmail;
        $this->customerIp = $customerIp;
    }

    /**
     * @return string|null
     */
    public function getBillingAddress(): ?string
    {
        return $this->billingAddress;
    }

    /**
     * @return string|null
     */
    public function getCustomerName(): ?string
    {
        return $this->customerName;
    }

    /**
     * @return string|null
     */
    public function getCustomerEmail(): ?string
    {
        return $this->customerEmail;
    }

    /**
     * @return string|null
     */
    public function getCustomerIp(): ?string
    {
        return $this->customerIp;
    }
}
