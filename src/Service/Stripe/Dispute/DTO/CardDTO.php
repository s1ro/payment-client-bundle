<?php

namespace AppPaymentClient\Service\Stripe\Dispute\DTO;

class CardDTO
{
    /**
     * @var string|null
     */
    private $brand;
    /**
     * @var string|null
     */
    private $networkReasonCode;

    public function __construct(?string $brand, ?string $networkReasonCode)
    {
        $this->brand = $brand;
        $this->networkReasonCode = $networkReasonCode;
    }

    /**
     * @return string|null
     */
    public function getBrand(): ?string
    {
        return $this->brand;
    }

    /**
     * @return string|null
     */
    public function getNetworkReasonCode(): ?string
    {
        return $this->networkReasonCode;
    }
}
