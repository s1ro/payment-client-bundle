<?php

namespace AppPaymentClient\Service\Stripe\Dispute;

use AppPaymentClient\Service\AbstractClient;
use AppPaymentClient\Service\ServiceNameProvider;
use AppPaymentClient\Service\Stripe\Dispute\DTO\CardDTO;
use AppPaymentClient\Service\Stripe\Dispute\DTO\DisputeDTO;
use AppPaymentClient\Service\Stripe\Dispute\DTO\EvidenceDTO;
use AppPaymentClient\Service\Stripe\Dispute\DTO\PaymentMethodDetailsDTO;
use AppPaymentClient\Service\Stripe\Dispute\Exception\StripeDisputeException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class StripeDisputeService extends AbstractClient implements StripeDisputeServiceInterface
{
    private const BASE_URL = '/api/stripe/dispute';

    /**
     * @var HttpClientInterface
     */
    private $httpClient;
    /**
     * @var ServiceNameProvider
     */
    private $serviceNameProvider;

    public function __construct(HttpClientInterface $httpClient, ServiceNameProvider $serviceNameProvider)
    {
        $this->httpClient = $httpClient;
        $this->serviceNameProvider = $serviceNameProvider;
    }

    /**
     * @inheritDoc
     */
    public function getDisputesByOrder(int $orderId, int $planId): array
    {
        try {
            $response = $this->httpClient->request(
                'GET',
                $this->getAppPaymentsUrl() . self::BASE_URL . '/order/' . $orderId . '/plan/' . $planId,
                ['query' => ['service_name' => $this->serviceNameProvider->getServiceName()]]
            );
            $data = json_decode($response->getContent(false), true);
        } catch (\Throwable $t) {
            throw StripeDisputeException::fromThrowable($t);
        }
        if (isset($data['error']) && $data['error']) {
            throw StripeDisputeException::create($data['message'] ?? 'Stripe error');
        }
        if (!isset($data['disputes'])) {
            throw StripeDisputeException::create('Invalid response from stripe');
        }
        $result = [];
        foreach ($data['disputes'] as $dispute) {
            $result[] = $this->buildDisputeDTO($dispute);
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getDisputeById(string $id, bool $test = false): ?DisputeDTO
    {
        try {
            $response = $this->httpClient->request(
                'GET',
                $this->getAppPaymentsUrl() . self::BASE_URL . "/$id",
                ['query' => [
                    'service_name' => $this->serviceNameProvider->getServiceName(),
                    'test' => (int) $test,
                ]]
            );
            $data = json_decode($response->getContent(false), true);
        } catch (\Throwable $t) {
            throw StripeDisputeException::fromThrowable($t);
        }
        if (!isset($data['dispute'])) {
            return null;
        }
        return $this->buildDisputeDTO($data['dispute']);
    }

    private function buildDisputeDTO(array $dispute): DisputeDTO
    {
        $evidence = $dispute['evidence'] ?? null;
        $paymentMethodDetails = $dispute['payment_method_details'] ?? null;
        $card = $paymentMethodDetails['card'] ?? null;
        return new DisputeDTO(
            $dispute['id'],
            $dispute['amount'],
            (new \DateTime())->setTimestamp($dispute['created']),
            $dispute['currency'],
            new EvidenceDTO(
                $evidence['billing_address'] ?? null,
                $evidence['customer_name'] ?? null,
                $evidence['customer_email'] ?? null,
                $evidence['customer_ip'] ?? null
            ),
            $dispute['payment_intent_id'],
            is_null($paymentMethodDetails)
                ? null
                : new PaymentMethodDetailsDTO(
                is_null($card)
                    ? null
                    : new CardDTO(
                    $card['brand'] ?? null,
                    $card['network_reason_code'] ?? null
                ),
                $paymentMethodDetails['type'] ?? null
            ),
            $dispute['reason'],
            $dispute['status']
        );
    }
}
