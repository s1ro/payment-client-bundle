<?php

namespace AppPaymentClient\Service\Stripe\Dispute\Exception;

class StripeDisputeException extends \Exception
{
    /**
     * @param string $message
     * @return self
     */
    public static function create(string $message): self
    {
        return new self($message);
    }

    /**
     * @param \Throwable $t
     * @return self
     */
    public static function fromThrowable(\Throwable $t): self
    {
        return new self($t->getMessage(), $t->getCode(), $t);
    }
}
