<?php

namespace AppPaymentClient\Service\Stripe\Dispute;

interface DisputeStatus
{
    public const LOST = 'lost';
    public const WON = 'won';
    public const WARNING_CLOSED = 'warning_closed';
    public const CLOSED_STATUSES = [self::LOST, self::WON, self::WARNING_CLOSED];
}
