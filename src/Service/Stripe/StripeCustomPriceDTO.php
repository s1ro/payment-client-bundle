<?php

namespace AppPaymentClient\Service\Stripe;

use AppPaymentClient\Entity\StripeIntervalInterface;

class StripeCustomPriceDTO
{
    /**
     * @var float
     */
    private $customPrice;
    /**
     * @var string
     * @see StripeIntervalInterface
     */
    private $interval;

    public function __construct(
        float $customPrice,
        string $interval = StripeIntervalInterface::MONTH
    )
    {
        $this->customPrice = $customPrice;
        $this->interval = $interval;
    }

    /**
     * @return float
     */
    public function getCustomPrice(): float
    {
        return $this->customPrice;
    }

    /**
     * @return string
     */
    public function getInterval(): string
    {
        return $this->interval;
    }
}