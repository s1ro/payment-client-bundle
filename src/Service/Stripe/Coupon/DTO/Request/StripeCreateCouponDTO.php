<?php

namespace AppPaymentClient\Service\Stripe\Coupon\DTO\Request;

class StripeCreateCouponDTO
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var float
     */
    private $value;
    /**
     * @var bool
     */
    private $test;
    /**
     * @var int|null
     */
    private $maxRedemptions;
    /**
     * @var \DateTimeInterface|null
     */
    private $redeemBy;

    public function __construct(
        string $id,
        string $name,
        float $value,
        bool $test = false,
        ?int $maxRedemptions = null,
        ?\DateTimeInterface $redeemBy = null
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->value = $value;
        $this->test = $test;
        $this->maxRedemptions = $maxRedemptions;
        $this->redeemBy = $redeemBy;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @return bool
     */
    public function getTest(): bool
    {
        return $this->test;
    }

    /**
     * @return int|null
     */
    public function getMaxRedemptions(): ?int
    {
        return $this->maxRedemptions;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getRedeemBy(): ?\DateTimeInterface
    {
        return $this->redeemBy;
    }

    /**
     * @param string $id
     * @param string $name
     * @param float $value
     * @param bool $test
     * @param int|null $maxRedemptions
     * @param \DateTimeInterface|null $redeemBy
     * @return static
     */
    public static function make(
        string $id,
        string $name,
        float $value,
        bool $test = false,
        ?int $maxRedemptions = null,
        ?\DateTimeInterface $redeemBy = null
    ): self
    {
        return new self($id, $name, $value, $test, $maxRedemptions, $redeemBy);
    }
}
